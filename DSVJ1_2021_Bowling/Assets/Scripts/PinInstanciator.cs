﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinInstanciator : MonoBehaviour
{
    public GameObject pins;

    public List<GameObject> kegels = new List<GameObject>();

    public float offset, heightY;

    [SerializeField]
    PinsScoreManager manager;

    void Start()
    {
        GameObject pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[0].transform.position = transform.position;
        kegels[0].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[1].transform.position = new Vector3(transform.position.x-offset,transform.position.y, transform.position.z - (offset * Mathf.Sqrt(2)));
        kegels[1].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[2].transform.position = new Vector3(transform.position.x - offset, transform.position.y, transform.position.z + (offset * Mathf.Sqrt(2)));
        kegels[2].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[3].transform.position = new Vector3(transform.position.x - offset * 2, transform.position.y, transform.position.z - ((offset * Mathf.Sqrt(2) )*2));
        kegels[3].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[4].transform.position = new Vector3(transform.position.x - offset * 2, transform.position.y, transform.position.z);
        kegels[4].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[5].transform.position = new Vector3(transform.position.x - offset * 2, transform.position.y, transform.position.z + ((offset * Mathf.Sqrt(2)*2)));
        kegels[5].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[6].transform.position = new Vector3(transform.position.x - offset * 3, transform.position.y, transform.position.z - ((offset * Mathf.Sqrt(2)) * 3));
        kegels[6].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[7].transform.position = new Vector3(transform.position.x - offset * 3, transform.position.y, transform.position.z - ((offset * Mathf.Sqrt(2))));
        kegels[7].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[8].transform.position = new Vector3(transform.position.x - offset * 3, transform.position.y, transform.position.z + ((offset * Mathf.Sqrt(2))));
        kegels[8].transform.parent = this.gameObject.transform;

        pin = Instantiate(pins).gameObject;
        kegels.Add(pin);
        kegels[9].transform.position = new Vector3(transform.position.x - offset * 3, transform.position.y, transform.position.z +((offset * Mathf.Sqrt(2)) * 3));
        kegels[9].transform.parent = this.gameObject.transform;

        transform.Rotate(0, 90, 0);
    }
}
