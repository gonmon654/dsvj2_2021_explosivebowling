﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallInstanciate : MonoBehaviour
{
    public GameObject ballPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(ballPrefab, transform.position, transform.rotation);
    }
}
