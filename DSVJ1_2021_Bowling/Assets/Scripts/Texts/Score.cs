﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    public int score = 0;
    public TextMeshProUGUI textScore;

    public void Add(float force)
    {
        UpdateDisplay(force);
    }

    private void Start()
    {
        score = 0;
    }

    void UpdateDisplay(float force)
    {
        textScore.text = "Force " + force;
    }
}
