﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    [SerializeField] GameObject[] buttonsMainMenu;

    private void Update()
    {
        for (int i = 0; i < buttonsMainMenu.Length; i++)
        {
            if (Input.GetMouseButtonDown(0))
            {
                switch (i)
                {
                    case 0:
                        SceneManager.LoadScene("Bowling In Game");
                        break;
                    case 1:
                        SceneManager.LoadScene("Bowling Explosive In Game");
                        break;
                    default:
                        SceneManager.LoadScene("Bowling In Game");
                        break;
                }
            }
        }
    }
}
